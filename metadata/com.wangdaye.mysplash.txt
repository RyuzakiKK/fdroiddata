Categories:Multimedia
License:LGPL-3.0
Web Site:https://github.com/WangDaYeeeeee/Mysplash/blob/HEAD/README.md
Source Code:https://github.com/WangDaYeeeeee/Mysplash
Issue Tracker:https://github.com/WangDaYeeeeee/Mysplash/issues

Auto Name:Mysplash
Summary:View stream of high resolution images
Description:
A lightweight client for [https://unsplash.com/ Unsplash] with following
features:

* 100000+ photos
* material design
* select photos as tags
* share photos and set as wallpaper
* download raw photos.
.

Repo Type:git
Repo:https://github.com/WangDaYeeeeee/Mysplash

Build:2.7.3,273
    commit=a6bd95b4b9d3e52bd50dc51286700ad130efb3d8
    subdir=app
    gradle=yes

Build:2.7.8,278
    commit=7c2d7bf299d5b6aa0b62717b337b4810532a90aa
    subdir=app
    gradle=yes

Build:2.7.9,279
    commit=ae4adcddb6019e0f41dca6902423cb794aba6a8c
    subdir=app
    gradle=yes

Build:2.8.3,283
    commit=c80c502c2209fd5dc2063a1d723811b54036a683
    subdir=app
    gradle=yes

Build:2.8.4,284
    commit=0f843aaf3988c63fd8cf12593355c459ab087b0e
    subdir=app
    gradle=yes

Build:2.8.8,288
    commit=2.8.8
    subdir=app
    gradle=yes

Build:2.8.9,289
    commit=2.8.9
    subdir=app
    gradle=yes

Auto Update Mode:Version %v
Update Check Mode:Tags
Current Version:2.8.9
Current Version Code:289
